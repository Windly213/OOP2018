package hk.edu.polyu.comp.comp2021.jungle;

enum commandsEnum {
    /*piece enum*/
    SAVE("save"), MOVE("move"), OPEN("open");
    private String value;

    commandsEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}

//public class Command {
//    public boolean save(){}
//    public  boolean open(){}
//    public boolean move(){}
//}
