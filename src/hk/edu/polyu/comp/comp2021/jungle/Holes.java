package hk.edu.polyu.comp.comp2021.jungle;

public class Holes {
    public static boolean squareInSelfHole(Square square) {
        if (square.getColor() == Player.black) {
            return square.getX() == 3 && square.getY() == 0;
        } else if (square.getColor() == Player.red) {
            return square.getX() == 3 && square.getY() == 8;
        }
        return false;
    }
}
