package hk.edu.polyu.comp.comp2021.jungle;


enum enumPieceValue {
    /*piece enum*/
    Elephant(8), Lion(7), Tiger(6), Leopard(5), Wolf(4), Dog(3), Cat(2), Mouse(1);
    private int value;

    public int getValue() {
        return this.value;
    }

    enumPieceValue(int value) {
        this.value = value;
    }
}

public class prioritizePieces {

    //0 not eatable, 1 eatable
    public static int compare2Piece(Square sq1, Square sq2) {
        String type1 = null, type2 = null;
        if (inTrap(sq1)) {
            return -1;
        }
        if (inTrap(sq2)) {
            return 1;
        }
        if (sq1.piece.getType() != null) {
            type1 = sq1.piece.getType();
        }
        if (sq2.piece.getType() != null) {
            type2 = sq2.piece.getType();
        }
        if (type1 == null || type2 == null) {
            System.exit(1);
        }
        if (type1.equals(type2)) {
            return 0;
        } else {
            if (type1.equals("Elephant") && type2.equals("Mouse")) {
                return -1;
            } else if (type1.equals("Mouse") && type2.equals("Elephant")) {
                return 1;
            } else if (enumPieceValue.valueOf(type1).getValue() < enumPieceValue.valueOf(type2).getValue()) {
                return -1;
            } else {
                return 1;
            }
        }
    }

    public static boolean eatable(Square square1, Square square2) {
        if (compare2Piece(square1, square2) == 1 || compare2Piece(square1, square2) == 0) {
            return true;
        }
        return false;
    }

    private static boolean inTrap(Square square) {
        if (square.getColor() == Player.black) {
            if (square.getY() == 8) {
                if (square.getX() == 2 || square.getX() == 4)
                    return true;
            } else if (square.getX() == 3 && square.getY() == 7) {
                return true;
            }
        } else if (square.getColor() == Player.red) {
            if (square.getY() == 0) {
                if (square.getX() == 2 || square.getX() == 4) {
                    return true;
                }
            }
            else if (square.getX() == 3 && square.getY() == 1) {
                return true;
            }
        }
        return false;
    }
}
