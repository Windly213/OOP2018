package hk.edu.polyu.comp.comp2021.jungle;


enum riverAbility {
    /*piece enum*/
    ELEPHANT(false), LION(true), TIGER(true), LEOPARD(false), WOLF(false), DOG(false), CAT(false), MOUSE(false);
    private boolean value;

    public boolean getValue() {
        return this.value;
    }

    riverAbility(boolean value) {
        this.value = value;
    }
}

public class jumpRiver {
    public static boolean canJump(Square sq) {
        boolean res = riverAbility.valueOf(sq.getPieceType()).getValue();
        return res;
    }

    public static boolean waterOnecrossable(Square[][] board) {
        for (int i = 1; i < 2; i++) {
            for (int j = 3; j < 6; j++) {
                if (board[i][j].getPieceType() != null && board[i][j].getPieceType().equals("Mouse")) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean waterTwocrossable(Square[][] board) {
        for (int i = 4; i < 5; i++) {
            for (int j = 3; j < 6; j++) {
                if (board[i][j].getPieceType() != null && board[i][j].getPieceType().equals("Mouse")) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean squareInRiver(Square square) {
        if (square.getX() == 1 || square.getX() == 2) {
            return ifInRiver(square);
        }
        if (square.getX() == 4 || square.getX() == 5) {
            return ifInRiver(square);
        }
        return false;
    }

    private static boolean ifInRiver(Square square) {
        switch (square.getY()) {
            case 3:
                return true;
            case 4:
                return true;
            case 5:
                return true;
            default:
                return false;
        }
    }

    public static Square coordinateConversion(Square movedSquare, Square originalSquare, boolean left) {
        Square tmpSquare = null;
        if (originalSquare.getX() == 1 && originalSquare.getY() == 2) {
            if (movedSquare.getX() == 1 && movedSquare.getY() == 3) {
                tmpSquare = new Square(1, 6);
            }
        } else if (originalSquare.getX() == 2 && originalSquare.getY() == 2) {
            if (movedSquare.getX() == 2 && movedSquare.getY() == 3) {
                tmpSquare = new Square(2, 6);
            }
        } else if (originalSquare.getX() == 4 && originalSquare.getY() == 2) {
            if (movedSquare.getX() == 4 && movedSquare.getY() == 3) {
                tmpSquare = new Square(4, 6);
            }
        } else if (originalSquare.getX() == 5 && originalSquare.getY() == 2) {
            if (movedSquare.getX() == 5 && movedSquare.getY() == 3) {
                tmpSquare = new Square(5, 6);
            }
        } else if (originalSquare.getX() == 0 && originalSquare.getY() == 3) {
            if (movedSquare.getX() == 1 && movedSquare.getY() == 3) {
                tmpSquare = new Square(3, 3);
            }
        } else if (originalSquare.getX() == 0 && originalSquare.getY() == 4) {
            if (movedSquare.getX() == 1 && movedSquare.getY() == 4) {
                tmpSquare = new Square(3, 4);
            }
        } else if (originalSquare.getX() == 0 && originalSquare.getY() == 5) {
            if (movedSquare.getX() == 1 && movedSquare.getY() == 5) {
                tmpSquare = new Square(3, 5);
            }
        } else if (originalSquare.getX() == 6 && originalSquare.getY() == 3) {
            if (movedSquare.getX() == 5 && movedSquare.getY() == 3) {
                tmpSquare = new Square(3, 3);
            }
        } else if (originalSquare.getX() == 6 && originalSquare.getY() == 4) {
            if (movedSquare.getX() == 5 && movedSquare.getY() == 4) {
                tmpSquare = new Square(3, 4);
            }
        } else if (originalSquare.getX() == 6 && originalSquare.getY() == 5) {
            if (movedSquare.getX() == 5 && movedSquare.getY() == 5) {
                tmpSquare = new Square(3, 5);
            }
        } else if (originalSquare.getX() == 1 && originalSquare.getY() == 6) {
            if (movedSquare.getX() == 1 && movedSquare.getY() == 5) {
                tmpSquare = new Square(1, 2);
            }
        } else if (originalSquare.getX() == 2 && originalSquare.getY() == 6) {
            if (movedSquare.getX() == 2 && movedSquare.getY() == 5) {
                tmpSquare = new Square(2, 2);
            }
        } else if (originalSquare.getX() == 4 && originalSquare.getY() == 6) {
            if (movedSquare.getX() == 4 && movedSquare.getY() == 5) {
                tmpSquare = new Square(4, 2);
            }
        } else if (originalSquare.getX() == 5 && originalSquare.getY() == 6) {
            if (movedSquare.getX() == 5 && movedSquare.getY() == 5) {
                tmpSquare = new Square(5, 2);
            }
        } else if (left) {
            switch (originalSquare.getY()) {
                case 3:
                    tmpSquare = new Square(0, originalSquare.getY());
                    break;
                case 4:
                    tmpSquare = new Square(0, originalSquare.getY());
                    break;
                case 5:
                    tmpSquare = new Square(0, originalSquare.getY());
                    break;
            }
        } else {
            switch (originalSquare.getY()) {
                case 3:
                    tmpSquare = new Square(6, originalSquare.getY());
                    break;
                case 4:
                    tmpSquare = new Square(6, originalSquare.getY());
                    break;
                case 5:
                    tmpSquare = new Square(6, originalSquare.getY());
                    break;
            }
        }
        return tmpSquare;

    }
}
