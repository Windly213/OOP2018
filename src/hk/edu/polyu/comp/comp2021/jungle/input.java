package hk.edu.polyu.comp.comp2021.jungle;

import java.util.Scanner;

public class input {
    public static void setName(ChessBoard board, String redName, String blackName) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("input redPlayer name: \n");
        redName = scanner.nextLine();
        System.out.println("input black player name\n");
        blackName = scanner.nextLine();

        if (isSameName(redName, blackName)) {
            System.out.println("Same name received. Exiting....");
            System.exit(1);
        }

        board.setRedPlayerName(redName);
        board.setBlackPlayerName(blackName);
    }

    public static boolean isSameName(String redName, String blackName) {
        return redName.equals(blackName);
    }
}
