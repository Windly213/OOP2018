package hk.edu.polyu.comp.comp2021.jungle;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class ChessPanel extends JPanel implements MouseListener {
    private static int SQUARE_SIZE = 64;
    private ChessBoard board;
    private Square clickedSquare;
    private ArrayList<Square> possibleMoves;
    private Player playerToMove;

    public ChessPanel(ChessBoard board) {
        super();
        this.addMouseListener(this);
        this.board = board;
        this.clickedSquare = null;
        this.possibleMoves = new ArrayList<Square>();
        this.playerToMove = Player.red;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        this.paintBoard(g);
        this.paintPieces(g);
    }

    private void paintPieces(Graphics g) {
        for (Piece p : board.getRedPieces()) {
            paintPiece(g, p.getType(), false, SQUARE_SIZE * p.getX(), SQUARE_SIZE * p.getY());
        }

        for (Piece p : board.getBlackPieces()) {
            paintPiece(g, p.getType(), true, SQUARE_SIZE * p.getX(), SQUARE_SIZE * p.getY());
        }
    }

    private void paintPiece(Graphics g, String pieceType, boolean isWhite, int x, int y) {
        String color = "";
        if (!isWhite) color = "red";
        if (isWhite) color = "black";
        try {
            g.drawImage(ImageIO.read(new File("src/hk/edu/polyu/comp/comp2021/jungle/graphics/" + color + "_" + pieceType.toLowerCase() + ".png")), x, y, SQUARE_SIZE, SQUARE_SIZE, null);
        } catch (IOException e) {
            System.out.println("graphics/" + color + "_" + pieceType.toLowerCase() + ".png not found");
        }
    }

    private void paintBoard(Graphics g) {
        g.setColor(Color.WHITE);
        int x, y;
        for (x = 0; x < 7; x++) {
            for (y = 0; y < 9; y++) {
                drawSquare(x, y, g);
            }
        }
    }

    private void drawSquare(int x, int y, Graphics g) {
        if ((x + y) % 2 == 0) {
            g.setColor(Color.white);
        }
        if ((x + y) % 2 == 1) {
            g.setColor(Color.gray);
        }
        if ((x == 2 && y == 0) || (x == 4 && y == 0) || (x == 3 && y == 1)) {
            g.setColor(Color.blue);
        } else if ((x == 3 && y == 7) || (x == 2 && y == 8) || (x == 4 && y == 8)) {
            g.setColor(Color.blue);
        } else if ((x == 3 && y == 0) || (x == 3 && y == 8)) {
            g.setColor(Color.green);
        } else if ((x == 1 && y == 3) || (x == 2 && y == 3) || (x == 4 && y == 3) || (x == 5 && y == 3)) {
            g.setColor(Color.orange);
        } else if ((x == 1 && y == 4) || (x == 2 && y == 4) || (x == 4 && y == 4) || (x == 5 && y == 4)) {
            g.setColor(Color.orange);
        } else if ((x == 1 && y == 5) || (x == 2 && y == 5) || (x == 4 && y == 5) || (x == 5 && y == 5)) {
            g.setColor(Color.orange);
        }
        if (clickedSquare != null) {
            if (board.getSquare(x, y, board.getSquares()) == clickedSquare) {
                g.setColor(new Color(160, 40, 200));
            }
            for (Square sq : possibleMoves) {
                if (sq.getX() == x && sq.getY() == y) {
                    if ((x + y) % 2 == 0) {
                        g.setColor(new Color(10, 200, 200));
                    }
                    if ((x + y) % 2 == 1) {
                        g.setColor(new Color(80, 100, 200));
                    }
                }

            }
        }
        g.fillRect(SQUARE_SIZE * x, SQUARE_SIZE * y, SQUARE_SIZE, SQUARE_SIZE);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }


    @Override
    public void mousePressed(MouseEvent e) {
        Square oldSquare = this.clickedSquare;
        this.clickedSquare = board.getSquare(e.getX() / 64, e.getY() / 64, board.getSquares());
//        System.out.println("clicked square: " + clickedSquare.getX() + " " + clickedSquare.getY());
//        System.out.println("e " + e.getX() + " " + e.getY());
//        System.out.println("x/y64 " + e.getX() / 64 + " " + e.getY() / 64);
        boolean flag = false;
        flag = containedSituation(oldSquare, flag);
        notContainedSituation(flag);
        repaint();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    public void actionPerformed(ActionEvent e) {
        String str = GUI.getTextField();
        String trimedStr = str.trim();
        String[] splitStr = trimedStr.split("\\s+");
        String Command = splitStr[0], argv1 = splitStr[1];
        if (splitStr.length > 3) {
            System.out.println("Error, re-enter");
            return;
        }
        if (Command.equals("save")) {
            board.save(argv1);
        } else if (Command.equals("load")) {
            board.load(argv1);
        } else if (Command.equals("move")) {
            Square oldSquare = this.clickedSquare;
            this.clickedSquare = board.getSquare(inputFirstCoorConversion(argv1.charAt(0)), inputSecondCoorConversion(argv1.charAt(1)), board.getSquares());
            boolean containFlag = false;
            containFlag = containedSituation(oldSquare, containFlag);
            notContainedSituation(containFlag);
        } else {
            System.out.println("Commands cannot be recognised.");
            ChessBoard.printBoard(board.getSquares());
        }
        repaint();

    }


    private void notContainedSituation(boolean containFlag) {
        if (containFlag == false) {
            this.possibleMoves.clear();
            if (clickedSquare.getPieceType() != null) {
                if (clickedSquare.getColor() == this.playerToMove) {
                    try {
                        this.possibleMoves = board.findRunnableMoves(clickedSquare, board.getSquares());
                    } catch (ClassNotFoundException e1) {
                        e1.printStackTrace();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        }
    }

    private boolean containedSituation(Square oldSquare, boolean containFlag) {
        for (Square square : possibleMoves) {
            if (this.clickedSquare.getX() == square.getX() && this.clickedSquare.getY() == square.getY()) {
                containFlag = true;
                this.board.makeMove(oldSquare, clickedSquare);
                this.possibleMoves.clear();
                if (playerToMove == Player.red) {
                    System.out.println("\n Player:" + playerToMove.toString() + " " + ChessBoard.getRedPlayerName() + " has moved\n");
                } else {
                    System.out.println("\n Player:" + playerToMove.toString() + " " + ChessBoard.getBlackPlayerName() + " has moved\n");
                }
                System.out.println("------------------------------------------------------------------------");
                ChessBoard.printBoard(board.getSquares());
                System.out.println("------------------------------------------------------------------------");
                //exchange turn
                if (playerToMove == Player.red) playerToMove = Player.black;
                else playerToMove = Player.red;
                break;
            }
        }
        return containFlag;
    }

    private int inputFirstCoorConversion(char argv) {
        switch (argv) {
            case 'A':
                return 0;
            case 'B':
                return 1;
            case 'C':
                return 2;
            case 'D':
                return 3;
            case 'E':
                return 4;
            case 'F':
                return 5;
            case 'G':
                return 6;
            default:
                return -1;
        }

    }

    private int inputSecondCoorConversion(char argument) {
        switch (argument) {
            case '9':
                return 0;
            case '8':
                return 1;
            case '7':
                return 2;
            case '6':
                return 3;
            case '5':
                return 4;
            case '4':
                return 5;
            case '3':
                return 6;
            case '2':
                return 7;
            case '1':
                return 8;
            default:
                return -1;
        }

    }
}
