package hk.edu.polyu.comp.comp2021.jungle;

import javax.swing.*;
import java.awt.*;

public class GUI extends JFrame {
    private static JTextField inputField;
    JPanel components = new JPanel(new GridLayout());

    public GUI() {
        this.setTitle("Jungle Game");
        String redPlayerName = "";
        String blackPlayerName = "";

        ChessBoard chessBoard = new ChessBoard();
        ChessPanel chessPanel = new ChessPanel(chessBoard);
        input.setName(chessBoard, redPlayerName, blackPlayerName);

        inputField = new JTextField();
        JButton jButton = new JButton("Confirm");
        JLabel jLabel = new JLabel("Instruction: 00");
        jButton.setBounds(450, 140, 100, 40);
        inputField.setBounds(450, 20, 120, 100);
        jButton.addActionListener(chessPanel::actionPerformed);

        chessPanel.setLayout(null);
        chessPanel.add(jButton);
        chessPanel.add(inputField);


        components.add(chessPanel);
        this.add(components);

        this.setSize(600, 700);
        this.setVisible(true);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        new GUI();
    }

    public static String getTextField() {
        return inputField.getText();
    }

}
