package hk.edu.polyu.comp.comp2021.jungle;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class ChessBoard {
    public static final int BOARDWIDTH = 7;
    public static final int BOARDLENGTH = 9;
    public static final int BOARDWIDTHEND = 6;
    public static final int BOARDLENGTHEND = 8;
    public static boolean endGame = false;
    private static String redPlayerName;
    private static String blackPlayerName;
    private ArrayList<Piece> blackPlayer;
    private ArrayList<Piece> redPlayer;
    private Square[][] squares;

    public ChessBoard() {
        this.squares = new Square[BOARDWIDTH][BOARDLENGTH];
        for (int x = 0; x < BOARDWIDTH; x++) {
            for (int y = 0; y < BOARDLENGTH; y++) {
                this.squares[x][y] = new Square(x, y);
            }
        }
        this.blackPlayer = new ArrayList<Piece>();
        this.redPlayer = new ArrayList<Piece>();
        setup();
    }

    public static void printBoard(Square[][] board) {
        for (int i = 0; i < BOARDLENGTH; i++) {
            for (int j = 0; j < BOARDWIDTH; j++) {
                if (!board[j][i].hasPiece()) {
                    System.out.print("-");
                    continue;
                }
                if (board[j][i].getColor() == Player.red) System.out.print("@");
                else System.out.print("X");
            }
            System.out.println();
        }
    }

    public static String getBlackPlayerName() {
        return blackPlayerName;
    }

    public void setBlackPlayerName(String blackPlayerName) {
        ChessBoard.blackPlayerName = blackPlayerName;
    }

    public static String getRedPlayerName() {
        return redPlayerName;
    }

    public void setRedPlayerName(String redPlayerName) {
        ChessBoard.redPlayerName = redPlayerName;
    }

    public static boolean boardIsChecked(Square[][] board, Player toMove) {
        if (board[3][0].hasPiece() || board[3][8].hasPiece()) {
            endGame = true;
            return true;
        }
        return false;
    }

    public static ArrayList<Square> findAllMoves(Square squareFrom, Square[][] boardState) {
        ArrayList<Square> possibleSquaresTo = new ArrayList<Square>();
        int x = squareFrom.getX();
        int y = squareFrom.getY();
        if (squareFrom.getPieceType().equals("Elephant")) {
            possibleSquaresTo.addAll(findElephantMoves(x, y, boardState));
        } else if (squareFrom.getPieceType().equals("Lion")) {
            possibleSquaresTo.addAll(findLionMoves(x, y, boardState));
        } else if (squareFrom.getPieceType().equals("Tiger")) {
            possibleSquaresTo.addAll(findTigerMoves(x, y, boardState));
        } else if (squareFrom.getPieceType().equals("Leopard")) {
            possibleSquaresTo.addAll(findLeopardMoves(x, y, boardState));
        } else if (squareFrom.getPieceType().equals("Wolf")) {
            possibleSquaresTo.addAll(findWolfMoves(x, y, boardState));
        } else if (squareFrom.getPieceType().equals("Dog")) {
            possibleSquaresTo.addAll(findDogMoves(x, y, boardState));
        } else if (squareFrom.getPieceType().equals("Cat")) {
            possibleSquaresTo.addAll(findCatMoves(x, y, boardState));
        } else if (squareFrom.getPieceType().equals("Mouse")) {
            possibleSquaresTo.addAll(findMouseMoves(x, y, boardState));
        }
        return possibleSquaresTo;
    }

    private static ArrayList<Square> findDogMoves(int x, int y, Square[][] boardState) {
        Player currTurn = Player.red;
        if (boardState[x][y].getColor() == Player.black) {
            currTurn = Player.black;
        }
        ArrayList<Square> moves = new ArrayList<Square>();
        int moveUpwards = -1, moveDownwards = 1, moveLeft = -1, moveRight = 1;

//        no piece
//        no river
//        no holes
//
        Square currentSquare = boardState[x][y], upMovedSquare = null, downMovedSquare = null, leftMovedSquare = null, rightMovedSquare = null;
        if (y + moveUpwards > -1) {
            upMovedSquare = boardState[x][y + moveUpwards];
        }
        if (y + moveDownwards < 9) {
            downMovedSquare = boardState[x][y + moveDownwards];
        }

        if (x + moveLeft > -1) {
            leftMovedSquare = boardState[x + moveLeft][y];
        }

        if (x + moveRight < 7) {
            rightMovedSquare = boardState[x + moveRight][y];
        }
        moveNormalAllowance(boardState, currTurn, moves, upMovedSquare);
        moveNormalAllowance(boardState, currTurn, moves, downMovedSquare);
        moveNormalAllowance(boardState, currTurn, moves, leftMovedSquare);
        moveNormalAllowance(boardState, currTurn, moves, rightMovedSquare);


//        // capturing
        normalCapturingAllowance(currTurn, moves, currentSquare, upMovedSquare);
        normalCapturingAllowance(currTurn, moves, currentSquare, downMovedSquare);
        normalCapturingAllowance(currTurn, moves, currentSquare, leftMovedSquare);
        normalCapturingAllowance(currTurn, moves, currentSquare, rightMovedSquare);
        return moves;
    }

    private static ArrayList<Square> findElephantMoves(int x, int y, Square[][] boardState) {
        Player currTurn = Player.red;
        if (boardState[x][y].getColor() == Player.black) {
            currTurn = Player.black;
        }

        ArrayList<Square> moves = new ArrayList<>();

        //going towards which side, elevate(-1), downwards +1
        int moveUpwards = -1, moveDownwards = 1, moveLeft = -1, moveRight = 1;

//        no piece
//        no river
//        no holes
//

        Square currentSquare = boardState[x][y], upMovedSquare = null, downMovedSquare = null, leftMovedSquare = null, rightMovedSquare = null;
        if (y + moveUpwards > -1) {
            upMovedSquare = boardState[x][y + moveUpwards];
        }
        if (y + moveDownwards < 9) {
            downMovedSquare = boardState[x][y + moveDownwards];
        }

        if (x + moveLeft > -1) {
            leftMovedSquare = boardState[x + moveLeft][y];
        }

        if (x + moveRight < 7) {
            rightMovedSquare = boardState[x + moveRight][y];
        }

        moveNormalAllowance(boardState, currTurn, moves, upMovedSquare);
        moveNormalAllowance(boardState, currTurn, moves, downMovedSquare);
        moveNormalAllowance(boardState, currTurn, moves, leftMovedSquare);
        moveNormalAllowance(boardState, currTurn, moves, rightMovedSquare);


//        // capturing
        normalCapturingAllowance(currTurn, moves, currentSquare, upMovedSquare);
        normalCapturingAllowance(currTurn, moves, currentSquare, downMovedSquare);
        normalCapturingAllowance(currTurn, moves, currentSquare, leftMovedSquare);
        normalCapturingAllowance(currTurn, moves, currentSquare, rightMovedSquare);

        return moves;
    }

    private static void moveNormalAllowance(Square[][] boardState, Player currTurn, ArrayList<Square> moves, Square MovedSquare) {
        if (MovedSquare != null && !MovedSquare.hasPiece()) {
            if (!jumpRiver.squareInRiver(MovedSquare)) {
                if (!Holes.squareInSelfHole(MovedSquare)) {
                    moves.add(MovedSquare);
                }
            }
        }
    }

    private static void normalCapturingAllowance(Player currTurn, ArrayList<Square> moves, Square currentSquare, Square MovedSquare) {
        if (MovedSquare != null && MovedSquare.hasPiece() && MovedSquare.getColor() != currTurn) {
            if (!jumpRiver.squareInRiver(MovedSquare)) {
                if (!Holes.squareInSelfHole(MovedSquare)) {
                    if (prioritizePieces.eatable(currentSquare, MovedSquare)) {
                        moves.add(MovedSquare);
                    }
                }
            }
        }
    }

    private static ArrayList<Square> findLionMoves(int x, int y, Square[][] boardState) {
        Player currTurn = Player.red;
        if (boardState[x][y].getColor() == Player.black) {
            currTurn = Player.black;
        }

        ArrayList<Square> moves = new ArrayList<Square>();
        //going towards which side, elevate(-1), downwards +1
        int moveUpwards = -1, moveDownwards = 1, moveLeft = -1, moveRight = 1;

//        no piece
//        no river
//        no holes
//
        Square currentSquare = boardState[x][y], upMovedSquare = null, downMovedSquare = null, leftMovedSquare = null, rightMovedSquare = null;
        if (y + moveUpwards > -1) {
            upMovedSquare = boardState[x][y + moveUpwards];
        }
        if (y + moveDownwards < 9) {
            downMovedSquare = boardState[x][y + moveDownwards];
        }

        if (x + moveLeft > -1) {
            leftMovedSquare = boardState[x + moveLeft][y];
        }

        if (x + moveRight < 7) {
            rightMovedSquare = boardState[x + moveRight][y];
        }


        //move

        moveLionTigerAllowance(boardState, moves, upMovedSquare, currentSquare, false);
        moveLionTigerAllowance(boardState, moves, downMovedSquare, currentSquare, false);
        moveLionTigerAllowance(boardState, moves, leftMovedSquare, currentSquare, true);
        moveLionTigerAllowance(boardState, moves, rightMovedSquare, currentSquare, false);

        //capturing
        capturingLionTigerAllowance(currTurn, moves, upMovedSquare, currentSquare, false);
        capturingLionTigerAllowance(currTurn, moves, downMovedSquare, currentSquare, false);
        capturingLionTigerAllowance(currTurn, moves, leftMovedSquare, currentSquare, true);
        capturingLionTigerAllowance(currTurn, moves, rightMovedSquare, currentSquare, false);

        return moves;
    }

    private static void capturingLionTigerAllowance(Player currTurn, ArrayList<Square> moves, Square MovedSquare, Square currentSquare, boolean ifLeft) {
        if (MovedSquare != null && MovedSquare.hasPiece() && MovedSquare.getColor() != currTurn) {
            if (!jumpRiver.squareInRiver(MovedSquare)) {
                //confirm it's river
                if (!Holes.squareInSelfHole(MovedSquare)) {
                    if (prioritizePieces.eatable(currentSquare, MovedSquare)) {
                        moves.add(MovedSquare);
                    }
                }
            } else if (jumpRiver.squareInRiver(MovedSquare)) {
                if (!Holes.squareInSelfHole(MovedSquare)) {
                    Square tmpSquare = jumpRiver.coordinateConversion(MovedSquare, currentSquare, ifLeft);
                    if (prioritizePieces.eatable(currentSquare, tmpSquare)) {
                        moves.add(tmpSquare);
                    }
                }
            }
        }
    }

    private static void moveLionTigerAllowance(Square[][] boardState, ArrayList<Square> moves, Square MovedSquare, Square originalSquare, boolean ifLeft) {
        if (MovedSquare != null && !MovedSquare.hasPiece()) {
            if (!jumpRiver.squareInRiver(MovedSquare)) {
                if (!Holes.squareInSelfHole(MovedSquare)) {
                    moves.add(MovedSquare);
                }
            } else if (jumpRiver.squareInRiver(MovedSquare)) {
                if (MovedSquare.getX() < 3) {
                    if (jumpRiver.waterOnecrossable(boardState)) {
                        if (!Holes.squareInSelfHole(MovedSquare)) {
                            Square tmpSquare = jumpRiver.coordinateConversion(MovedSquare, originalSquare, ifLeft);
                            MovedSquare = tmpSquare;
                            moves.add(MovedSquare);
                        }
                    }
                } else if (MovedSquare.getX() > 3) {
                    if (jumpRiver.waterTwocrossable(boardState)) {
                        if (!Holes.squareInSelfHole(MovedSquare)) {
                            Square tmpSquare = jumpRiver.coordinateConversion(MovedSquare, originalSquare, ifLeft);
                            MovedSquare = tmpSquare;
                            moves.add(MovedSquare);
                        }
                    }
                }

            }
        }
    }


    private static ArrayList<Square> findTigerMoves(int x, int y, Square[][] boardState) {
        Player currTurn = Player.red;
        if (boardState[x][y].getColor() == Player.black) {
            currTurn = Player.black;
        }

        ArrayList<Square> moves = new ArrayList<Square>();
        //going towards which side, elevate(-1), downwards +1
        int moveUpwards = -1, moveDownwards = 1, moveLeft = -1, moveRight = 1;

//        no piece
//        no river
//        no holes
//
        Square currentSquare = boardState[x][y], upMovedSquare = null, downMovedSquare = null, leftMovedSquare = null, rightMovedSquare = null;
        if (y + moveUpwards > -1) {
            upMovedSquare = boardState[x][y + moveUpwards];
        }
        if (y + moveDownwards < 9) {
            downMovedSquare = boardState[x][y + moveDownwards];
        }

        if (x + moveLeft > -1) {
            leftMovedSquare = boardState[x + moveLeft][y];
        }

        if (x + moveRight < 7) {
            rightMovedSquare = boardState[x + moveRight][y];
        }

        //move
        moveLionTigerAllowance(boardState, moves, upMovedSquare, currentSquare, false);
        moveLionTigerAllowance(boardState, moves, downMovedSquare, currentSquare, false);
        moveLionTigerAllowance(boardState, moves, leftMovedSquare, currentSquare, true);
        moveLionTigerAllowance(boardState, moves, rightMovedSquare, currentSquare, false);

        capturingLionTigerAllowance(currTurn, moves, upMovedSquare, currentSquare, false);
        capturingLionTigerAllowance(currTurn, moves, downMovedSquare, currentSquare, false);
        capturingLionTigerAllowance(currTurn, moves, leftMovedSquare, currentSquare, true);
        capturingLionTigerAllowance(currTurn, moves, rightMovedSquare, currentSquare, false);
        return moves;

    }

    private static ArrayList<Square> findLeopardMoves(int x, int y, Square[][] boardState) {
        Player currTurn = Player.red;
        if (boardState[x][y].getColor() == Player.black) {
            currTurn = Player.black;
        }
        ArrayList<Square> moves = new ArrayList<Square>();
        int moveUpwards = -1, moveDownwards = 1, moveLeft = -1, moveRight = 1;

//        no piece
//        no river
//        no holes
//
        Square currentSquare = boardState[x][y], upMovedSquare = null, downMovedSquare = null, leftMovedSquare = null, rightMovedSquare = null;
        if (y + moveUpwards > -1) {
            upMovedSquare = boardState[x][y + moveUpwards];
        }
        if (y + moveDownwards < 9) {
            downMovedSquare = boardState[x][y + moveDownwards];
        }

        if (x + moveLeft > -1) {
            leftMovedSquare = boardState[x + moveLeft][y];
        }

        if (x + moveRight < 7) {
            rightMovedSquare = boardState[x + moveRight][y];
        }

        moveNormalAllowance(boardState, currTurn, moves, upMovedSquare);
        moveNormalAllowance(boardState, currTurn, moves, downMovedSquare);
        moveNormalAllowance(boardState, currTurn, moves, leftMovedSquare);
        moveNormalAllowance(boardState, currTurn, moves, rightMovedSquare);


//        // capturing
        normalCapturingAllowance(currTurn, moves, currentSquare, upMovedSquare);
        normalCapturingAllowance(currTurn, moves, currentSquare, downMovedSquare);
        normalCapturingAllowance(currTurn, moves, currentSquare, leftMovedSquare);
        normalCapturingAllowance(currTurn, moves, currentSquare, rightMovedSquare);


        return moves;
    }

    private static ArrayList<Square> findWolfMoves(int x, int y, Square[][] boardState) {
        Player currTurn = Player.red;
        if (boardState[x][y].getColor() == Player.black) {
            currTurn = Player.black;
        }
        ArrayList<Square> moves = new ArrayList<Square>();
        int moveUpwards = -1, moveDownwards = 1, moveLeft = -1, moveRight = 1;

//        no piece
//        no river
//        no holes
//
        Square currentSquare = boardState[x][y], upMovedSquare = null, downMovedSquare = null, leftMovedSquare = null, rightMovedSquare = null;
        if (y + moveUpwards > -1) {
            upMovedSquare = boardState[x][y + moveUpwards];
        }
        if (y + moveDownwards < 9) {
            downMovedSquare = boardState[x][y + moveDownwards];
        }

        if (x + moveLeft > -1) {
            leftMovedSquare = boardState[x + moveLeft][y];
        }

        if (x + moveRight < 7) {
            rightMovedSquare = boardState[x + moveRight][y];
        }

        moveNormalAllowance(boardState, currTurn, moves, upMovedSquare);
        moveNormalAllowance(boardState, currTurn, moves, downMovedSquare);
        moveNormalAllowance(boardState, currTurn, moves, leftMovedSquare);
        moveNormalAllowance(boardState, currTurn, moves, rightMovedSquare);


//        // capturing
        normalCapturingAllowance(currTurn, moves, currentSquare, upMovedSquare);
        normalCapturingAllowance(currTurn, moves, currentSquare, downMovedSquare);
        normalCapturingAllowance(currTurn, moves, currentSquare, leftMovedSquare);
        normalCapturingAllowance(currTurn, moves, currentSquare, rightMovedSquare);

        return moves;
    }

    private static ArrayList<Square> findMouseMoves(int x, int y, Square[][] boardState) {
        Player currTurn = Player.red;
        if (boardState[x][y].getColor() == Player.black) {
            currTurn = Player.black;
        }
        ArrayList<Square> moves = new ArrayList<Square>();
        int moveUpwards = -1, moveDownwards = 1, moveLeft = -1, moveRight = 1;

//        no piece
//        no river
//        no holes
//
        Square currentSquare = boardState[x][y], upMovedSquare = null, downMovedSquare = null, leftMovedSquare = null, rightMovedSquare = null;
        if (y + moveUpwards > -1) {
            upMovedSquare = boardState[x][y + moveUpwards];
        }
        if (y + moveDownwards < 9) {
            downMovedSquare = boardState[x][y + moveDownwards];
        }

        if (x + moveLeft > -1) {
            leftMovedSquare = boardState[x + moveLeft][y];
        }

        if (x + moveRight < 7) {
            rightMovedSquare = boardState[x + moveRight][y];
        }
        moveMouseAllowance(boardState, currTurn, moves, upMovedSquare);
        moveMouseAllowance(boardState, currTurn, moves, downMovedSquare);
        moveMouseAllowance(boardState, currTurn, moves, leftMovedSquare);
        moveMouseAllowance(boardState, currTurn, moves, rightMovedSquare);


//        // capturing
        mouseCapturingAllowance(currTurn, moves, currentSquare, upMovedSquare);
        mouseCapturingAllowance(currTurn, moves, currentSquare, downMovedSquare);
        mouseCapturingAllowance(currTurn, moves, currentSquare, leftMovedSquare);
        mouseCapturingAllowance(currTurn, moves, currentSquare, rightMovedSquare);
        return moves;
    }

    private static void moveMouseAllowance(Square[][] boardState, Player currTurn, ArrayList<Square> moves, Square MovedSquare) {
        if (MovedSquare != null && !MovedSquare.hasPiece()) {
            if (!Holes.squareInSelfHole(MovedSquare)) {
                moves.add(MovedSquare);
            }
        }
    }

    private static void mouseCapturingAllowance(Player currTurn, ArrayList<Square> moves, Square currentSquare, Square MovedSquare) {
        if (MovedSquare != null && MovedSquare.hasPiece() && MovedSquare.getColor() != currTurn) {
            if (!jumpRiver.squareInRiver(MovedSquare) && !jumpRiver.squareInRiver(currentSquare)) {
                if (!Holes.squareInSelfHole(MovedSquare)) {
                    if (prioritizePieces.eatable(currentSquare, MovedSquare)) {
                        moves.add(MovedSquare);
                    }
                }
            } else if (jumpRiver.squareInRiver(MovedSquare) && jumpRiver.squareInRiver(currentSquare)) {
                if (!Holes.squareInSelfHole(MovedSquare)) {
                    if (prioritizePieces.eatable(currentSquare, MovedSquare)) {
                        moves.add(MovedSquare);
                    }
                }
            }
        }
    }

    private static ArrayList<Square> findCatMoves(int x, int y, Square[][] boardState) {
        Player currTurn = Player.red;
        if (boardState[x][y].getColor() == Player.black) {
            currTurn = Player.black;
        }
        ArrayList<Square> moves = new ArrayList<Square>();
        int moveUpwards = -1, moveDownwards = 1, moveLeft = -1, moveRight = 1;

//        no piece
//        no river
//        no holes
//
        Square currentSquare = boardState[x][y], upMovedSquare = null, downMovedSquare = null, leftMovedSquare = null, rightMovedSquare = null;
        if (y + moveUpwards > -1) {
            upMovedSquare = boardState[x][y + moveUpwards];
        }
        if (y + moveDownwards < 9) {
            downMovedSquare = boardState[x][y + moveDownwards];
        }

        if (x + moveLeft > -1) {
            leftMovedSquare = boardState[x + moveLeft][y];
        }

        if (x + moveRight < 7) {
            rightMovedSquare = boardState[x + moveRight][y];
        }
        moveNormalAllowance(boardState, currTurn, moves, upMovedSquare);
        moveNormalAllowance(boardState, currTurn, moves, downMovedSquare);
        moveNormalAllowance(boardState, currTurn, moves, leftMovedSquare);
        moveNormalAllowance(boardState, currTurn, moves, rightMovedSquare);


//        // capturing
        normalCapturingAllowance(currTurn, moves, currentSquare, upMovedSquare);
        normalCapturingAllowance(currTurn, moves, currentSquare, downMovedSquare);
        normalCapturingAllowance(currTurn, moves, currentSquare, leftMovedSquare);
        normalCapturingAllowance(currTurn, moves, currentSquare, rightMovedSquare);
        return moves;
    }

    public void save(String path) {
//        eg: "storedGame/game_storage.dat"
        File file = new File(path);
        try {
            if (file.getParentFile() != null) {
                file.getParentFile().mkdirs();
            }
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            PrintWriter writer = new PrintWriter(new FileOutputStream(file));
            for (Square[] square : squares
            ) {
                for (Square sq : square
                ) {
                    writer.println(sq.getX() + " " + sq.getY() + " " + sq.getColor() + " " + sq.getPieceType());
                }
            }
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    public Square[][] getSquares() {
        return squares;
    }

    public void load(String path) {
//        eg: path: "storedGame/game_storage.dat"
        try {
            File game_storage = new File(path);
            if (!game_storage.exists()) {
                throw new IOException();
            } else {
                List<String> list = Files.readAllLines(Paths.get(path));
                clear(squares);
                for (String str : list
                ) {
                    String[] splitedStr = str.split("\\s+");
                    String xCoor = splitedStr[0], yCoor = splitedStr[1], pieceColor = splitedStr[2], pieceType = splitedStr[3];
                    int parsedXCoor = Integer.parseInt(xCoor), parsedYCoor = Integer.parseInt(yCoor);
                    Piece piece;
                    if (pieceColor.equals("null")) {
                        continue;
                    } else if (Player.black.toString().equals(pieceColor)) {
                        piece = new Piece(Integer.parseInt(splitedStr[0]), Integer.parseInt(splitedStr[1]), pieceType, Player.black);
                        blackPlayer.add(piece);
                        getSquare(parsedXCoor, parsedYCoor, squares).placePiece(piece);
                    } else {
                        piece = new Piece(parsedXCoor, parsedYCoor, pieceType, Player.red);
                        redPlayer.add(piece);
                        getSquare(parsedXCoor, parsedYCoor, squares).placePiece(piece);
                    }

                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Square getSquare(int x, int y, Square[][] boardState) {
        return boardState[x][y];
    }

    public ArrayList<Piece> getBlackPieces() {
        return blackPlayer;
    }

    public ArrayList<Piece> getRedPieces() {
        return redPlayer;
    }

    public void setup() {
        this.clear(squares);

        int x, y;

        y = 6;

        for (x = 0; x < 7; x++) {
            Piece p = null;
            switch (x) {
                case 0:
                    p = new Piece(x, y, "Elephant", Player.red);
                    break;
                case 2:
                    p = new Piece(x, y, "Wolf", Player.red);
                    break;
                case 4:
                    p = new Piece(x, y, "Leopard", Player.red);
                    break;
                case 6:
                    p = new Piece(x, y, "Mouse", Player.red);
                    break;
                default:
                    continue;
            }
            this.redPlayer.add(p);
            squares[x][y].placePiece(p);
        }

        y = 7;
        //blackPlayer pawns
        for (x = 0; x < 7; x++) {
            Piece p = null;
            switch (x) {
                case 1:
                    p = new Piece(x, y, "Cat", Player.red);
                    break;
                case 5:
                    p = new Piece(x, y, "Dog", Player.red);
                    break;
                default:
                    continue;
            }
            this.redPlayer.add(p);
            squares[x][y].placePiece(p);
        }

        y = 8;
        for (x = 0; x < 7; x++) {
            Piece p = null;
            switch (x) {
                case 0:
                    p = new Piece(x, y, "Tiger", Player.red);
                    break;
                case 6:
                    p = new Piece(x, y, "Lion", Player.red);
                    break;
                default:
                    continue;
            }
            this.redPlayer.add(p);
            squares[x][y].placePiece(p);
        }

        y = 2;
        for (x = 0; x < 7; x++) {
            Piece p = null;
            switch (x) {
                case 6:
                    p = new Piece(x, y, "Elephant", Player.black);
                    break;
                case 4:
                    p = new Piece(x, y, "Wolf", Player.black);
                    break;
                case 2:
                    p = new Piece(x, y, "Leopard", Player.black);
                    break;
                case 0:
                    p = new Piece(x, y, "Mouse", Player.black);
                    break;
                default:
                    continue;
            }
            this.blackPlayer.add(p);
            squares[x][y].placePiece(p);
        }
        y = 1;

        for (x = 0; x < 7; x++) {
            Piece p = null;
            switch (x) {
                case 5:
                    p = new Piece(x, y, "Cat", Player.black);
                    break;
                case 1:
                    p = new Piece(x, y, "Dog", Player.black);
                    break;
                default:
                    continue;
            }
            this.blackPlayer.add(p);
            squares[x][y].placePiece(p);
        }
        y = 0;
        for (x = 0; x < 7; x++) {
            Piece p = null;
            switch (x) {
                case 6:
                    p = new Piece(x, y, "Tiger", Player.black);
                    break;
                case 0:
                    p = new Piece(x, y, "Lion", Player.black);
                    break;
                default:
                    continue;
            }
            this.blackPlayer.add(p);
            squares[x][y].placePiece(p);
        }
    }

    public void clear(Square[][] boardState) {

        for (int x = 0; x < BOARDWIDTH; x++) {
            for (int y = 0; y < BOARDLENGTH; y++) {
                boardState[x][y].clearSquare();
            }
        }

        blackPlayer.clear();
        redPlayer.clear();
    }

    public ArrayList<Square> findRunnableMoves(Square squareFrom, Square[][] boardState) throws
            ClassNotFoundException, IOException {
        ArrayList<Square> moves = findAllMoves(squareFrom, boardState);
        for (Square squareTo : moves) {
            if (checkMate(squareFrom, squareTo, boardState)) {
                System.out.println("Check! EndGame: " + squareFrom.getColor().toString() + " win");
            }
        }
        return moves;
    }

    public void makeMove(Square from, Square to) {
        Piece p = from.getPiece();

        if (to.hasPiece()) {
            this.blackPlayer.remove(to.getPiece());
            this.redPlayer.remove(to.getPiece());
            to.clearSquare();
        }


        to.placePiece(p);
        p.moveTo(to.getX(), to.getY());


        from.clearSquare();

    }

    private boolean checkMate(Square squareFrom, Square squareTo, Square[][] boardState) throws
            IOException, ClassNotFoundException {
        Player color = squareFrom.getColor();


        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(boardState);
        ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
        ObjectInputStream ois = new ObjectInputStream(bais);
        Square[][] copy = (Square[][]) ois.readObject();

        int xf = squareFrom.getX();
        int yf = squareFrom.getY();
        int xt = squareTo.getX();
        int yt = squareTo.getY();

        Square cloneFrom = copy[xf][yf];
        Square cloneTo = copy[xt][yt];

        Piece p = cloneFrom.getPiece();
        cloneTo.placePiece(p);
        cloneFrom.clearSquare();

        return boardIsChecked(copy, color);
    }

}
